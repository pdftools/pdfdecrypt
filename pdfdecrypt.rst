==========================
pdfdecrypt
==========================

-------------------------------------------------------------
Remove passwords from PDF documents
-------------------------------------------------------------

:Author:    Hartmut Goebel <h.goebel@crazy-compilers.com>
:Version:   Version |VERSION|
:Copyright: 2022 by Hartmut Goebel
:Licence:   GNU Affero General Public License v3 or later (AGPLv3+)
:Manual section: 1

.. raw:: manpage

   .\" disable justification (adjust text to left margin only)
   .ad l


SYNOPSIS
==========

``pdfdecrypt`` <options> InputFile OutputFile

DESCRIPTION
============

.. include:: docs/_description.txt

OPTIONS
========

.. include:: docs/_options.txt


EXAMPLES
============

.. include:: docs/_examples.txt


SEE ALSO
=============

``pdfposter``\(1) https://pdfposter.readthedocs.io/,
``pdfjoin``\(1) https://pdfjoin.readthedocs.io/,
``flyer-composer``\(1) http://www.crazy-compilers.com/flyer-composer.html


Project Homepage https://pdfdecrypt.readthedocs.io/
