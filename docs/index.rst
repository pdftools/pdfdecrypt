==============================================
pdfdecrypt
==============================================

.. container:: admonition topic

  **Remove passwords from PDF documents**

``pdfdecrypt`` is a command line tool and a Python library to
remove passwords from PDF documents.  It will read the encrypted
document, ask for the password and write the decrypted document
without touching any internal structure.


.. toctree::
   :maxdepth: 1

   Installation
   Usage
   Examples
   Donate <Donate>
   Frequently Asked Questions
   Changes
   Development

.. include:: _common_definitions.txt
